package com.umesh.movie.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 11:42 AM
 */

@Component
public class GenericUtil {

    public HttpHeaders getHttpHeaders() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return requestHeaders;
    }
}

package com.umesh.movie.model.admin;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 7:28 AM
 */

@Getter
@Setter
@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "movie_id", nullable = false)
    private Integer movieId;

    @Column(name = "movie_name")
    private String movieName;

    @Column(name = "seats", nullable = false)
    private Integer seats;

    @Column(name = "date", nullable = false)
    private Timestamp date;

    @Column(name = "created_at", nullable = false)
    private Timestamp createdAt;

    @Column(name = "status", nullable = false)
    private Integer status;
}

package com.umesh.movie.service;

import com.umesh.movie.dto.model.tmdb.MovieDto;
import com.umesh.movie.dto.model.tmdb.ListDto;
import com.umesh.movie.dto.model.tmdb.ReviewDto;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 11:30 AM
 */

public interface TmdbService {
    ListDto<MovieDto> getMoviesByDiscovery(int page);
    ListDto<MovieDto> getMoviesByQuery(int page, String query);
    MovieDto getMovieDetail(Long id);
    ListDto<ReviewDto> getMovieReviews(Long id);
}

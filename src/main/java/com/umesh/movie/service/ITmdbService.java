package com.umesh.movie.service;

import com.umesh.movie.dto.mapper.MovieDetailMapper;
import com.umesh.movie.dto.mapper.MovieListMapper;
import com.umesh.movie.dto.mapper.ReviewListMapper;
import com.umesh.movie.dto.model.tmdb.ListDto;
import com.umesh.movie.dto.model.tmdb.MovieDto;
import com.umesh.movie.dto.model.tmdb.ReviewDto;
import com.umesh.movie.model.tmdb.Movies;
import com.umesh.movie.model.tmdb.Result;
import com.umesh.movie.model.tmdb.Reviews;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 11:32 AM
 */

@Component
public class ITmdbService implements TmdbService {

    private static final Log LOGGER = LogFactory.getLog(ITmdbService.class);

    final RestTemplate restTemplate = new RestTemplate();

    @Value("${tmdb.base.url}")
    private String tmdbBaseUrl;

    @Value("${tmdb.api.key}")
    private String tmdbApiKey;

    @Value("${tmdb.content.language}")
    private String tmdbContentLanguage;

    @Override
    public ListDto<MovieDto> getMoviesByDiscovery(int page) {

        LOGGER.info("Movies list by discovery inbound!");
        ListDto<MovieDto> movieListDto = new ListDto<>();

        String url = tmdbBaseUrl + "/3/discover/movie";
        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("api_key", tmdbApiKey)
                .queryParam("language", tmdbContentLanguage)
                .queryParam("sort_by", "popularity.desc")
                .queryParam("include_adult", "false")
                .queryParam("include_video", "false")
                .queryParam("page", page)
                .queryParam("with_watch_monetization_types", "flatrate")
                .encode()
                .toUriString();

        try {
            ResponseEntity<Movies> response = restTemplate.getForEntity(urlTemplate, Movies.class);
            if (ObjectUtils.isEmpty(response) || ObjectUtils.isEmpty(response.getBody()))
                return movieListDto.setError("No response found!");

            LOGGER.info("Movies list by discovery completed!");
            return MovieListMapper.toMovieListDto(response.getBody());
        } catch (Exception e) {
            LOGGER.error("Error while fetching movie list: " + e.getMessage());
            return movieListDto.setError(e.getMessage());
        }

    }

    @Override
    public ListDto<MovieDto> getMoviesByQuery(int page, String query) {

        LOGGER.info("Movies list by query inbound! QUERY: " + query);
        ListDto<MovieDto> movieListDto = new ListDto<>();
        String url = tmdbBaseUrl + "/3/search/movie";
        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("api_key", tmdbApiKey)
                .queryParam("language", tmdbContentLanguage)
                .queryParam("sort_by", "popularity.desc")
                .queryParam("include_adult", "false")
                .queryParam("page", page)
                .queryParam("query", query)
                .build()
                .toUriString();

        try {
            ResponseEntity<Movies> response = restTemplate.getForEntity(urlTemplate, Movies.class);
            if (ObjectUtils.isEmpty(response) || ObjectUtils.isEmpty(response.getBody()))
                return movieListDto.setError("No response found!");

            LOGGER.info("Movies list by query completed! QUERY: " + query);
            return MovieListMapper.toMovieListDto(response.getBody());
        } catch (Exception e) {
            LOGGER.error("Error while fetching movie list: " + e.getMessage());
            return movieListDto.setError(e.getMessage());
        }
    }

    @Override
    public MovieDto getMovieDetail(Long id) {

        LOGGER.info("Movie detail inbound! ID: " + id);
        MovieDto movieDto = new MovieDto();

        String url = tmdbBaseUrl + "/3/movie/" + id;
        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("api_key", tmdbApiKey)
                .queryParam("language", tmdbContentLanguage)
                .encode()
                .toUriString();

        try {
            ResponseEntity<Result> response = restTemplate.getForEntity(urlTemplate, Result.class);
            if (ObjectUtils.isEmpty(response) || ObjectUtils.isEmpty(response.getBody()))
                return movieDto.setError("No response found!");

            LOGGER.info("Movie detail completed! ID: " + id);
            return MovieDetailMapper.toMovieDto(response.getBody());
        } catch (Exception e) {
            LOGGER.error("Error while fetching movie list: " + e.getMessage());
            return movieDto.setError(e.getMessage());
        }

    }

    @Override
    public ListDto<ReviewDto> getMovieReviews(Long id) {

        LOGGER.info("Movie reviews inbound! ID: " + id);
        ListDto<ReviewDto> reviewListDto = new ListDto<>();
        String url = tmdbBaseUrl + "/3/movie/" + id + "/reviews";
        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("api_key", tmdbApiKey)
                .queryParam("language", tmdbContentLanguage)
                .queryParam("page", "1")
                .encode()
                .toUriString();

        try {
            ResponseEntity<Reviews> response = restTemplate.getForEntity(urlTemplate, Reviews.class);
            if (ObjectUtils.isEmpty(response) || ObjectUtils.isEmpty(response.getBody()))
                return reviewListDto.setError("No response found!");

            LOGGER.info("Movie reviews completed! ID: " + id);
            return ReviewListMapper.toReviewListDto(response.getBody());
        } catch (Exception e) {
            LOGGER.error("Error while fetching movie list: " + e.getMessage());
            return reviewListDto.setError(e.getMessage());
        }

    }
}

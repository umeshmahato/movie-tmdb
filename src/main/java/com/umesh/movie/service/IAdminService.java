package com.umesh.movie.service;

import com.umesh.movie.controller.request.BookingListRequest;
import com.umesh.movie.controller.request.NewBookingRequest;
import com.umesh.movie.controller.request.UpdateBookingRequest;
import com.umesh.movie.dto.mapper.BookingListMapper;
import com.umesh.movie.dto.model.admin.BookingDto;
import com.umesh.movie.dto.model.tmdb.ListDto;
import com.umesh.movie.model.admin.Booking;
import com.umesh.movie.repository.BookingRepository;
import com.umesh.movie.specs.BookingSpecs;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 5:20 PM
 */

@Component
public class IAdminService implements AdminService {

    private static final Log LOGGER = LogFactory.getLog(IAdminService.class);

    @Autowired
    BookingRepository bookingRepository;

    @Override
    @Transactional
    public ListDto<BookingDto> fetchBookingList(BookingListRequest request) {

        LOGGER.info("Fetch bookings inbound!");
        ListDto<BookingDto> bookingListDto = new ListDto<>();
        try {
            PageRequest pageableSizeSortedByCreatedAtDesc = PageRequest.of(request.getOffset(), request.getPageSize(),
                    Sort.by("createdAt").descending());

            Page<Booking> bookings = bookingRepository.findAll(
                    Specification.where(BookingSpecs.requestFilters(request)),
                    pageableSizeSortedByCreatedAtDesc
            );
            LOGGER.info("Fetch bookings completed! Records: " + bookings.getTotalElements());
            return BookingListMapper.toBookingListDto(bookings);
        } catch (Exception e) {
            LOGGER.error("Error while fetching booking list: " + e.getMessage());
            return bookingListDto.setError(e.getMessage());
        }

    }

    @Override
    @Transactional
    public BookingDto updateBooking(Integer id, UpdateBookingRequest request) {

        LOGGER.info("Update booking inbound! ID: " + id);
        BookingDto bookingDto = new BookingDto();
        Optional<Booking> bookingLookup = bookingRepository.findById(id);
        if (bookingLookup.isPresent()) {
            Booking booking = bookingLookup.get();
            if (!ObjectUtils.isEmpty(request.getFirstName())) booking.setFirstName(request.getFirstName());
            if (!ObjectUtils.isEmpty(request.getLastName())) booking.setLastName(request.getLastName());
            if (!ObjectUtils.isEmpty(request.getEmail())) booking.setEmail(request.getEmail());
            if (!ObjectUtils.isEmpty(request.getStatus())) booking.setStatus(request.getStatus() ? 1 : 0);
            bookingRepository.saveAndFlush(booking);
            LOGGER.info("Update booking completed! ID: " + id);
            return bookingDto.setId(booking.getId());
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Booking not found!");

    }

    @Override
    @Transactional
    public BookingDto newBooking(NewBookingRequest request) {

        LOGGER.info("New booking inbound!");
        BookingDto bookingDto = new BookingDto();
        try {
            List<Booking> bookingList = bookingRepository.findByEmailAndMovieId(request.getEmail(), request.getMovieId());
            int existingTicketsForMovie = bookingList.stream()
                    .map(Booking::getSeats)
                    .reduce(0, Integer::sum);

            if (existingTicketsForMovie + request.getSeats() > 10)
                return bookingDto.setError("Max 10 tickets allowed!");

            Booking booking = new Booking();
            booking.setDate(new Timestamp(request.getDate().getTime()));
            booking.setCreatedAt(new Timestamp(new Date().getTime()));
            booking.setFirstName(request.getFirstName());
            booking.setLastName(request.getLastName());
            booking.setEmail(request.getEmail());
            booking.setMovieId(request.getMovieId());
            booking.setMovieName(request.getMovieName());
            booking.setSeats(request.getSeats());
            booking.setStatus(1);
            bookingRepository.saveAndFlush(booking);
            LOGGER.info("Update booking completed! ID: " + booking.getId());
            return bookingDto.setId(booking.getId());
        } catch (DataAccessException dae) {
            LOGGER.error("Error while saving booking: " + dae.getMessage());
            return bookingDto.setError(dae.getMessage());
        } catch (Exception e) {
            LOGGER.error("Exception while saving booking: " + e.getMessage());
            return bookingDto.setError(e.getMessage());
        }
    }
}

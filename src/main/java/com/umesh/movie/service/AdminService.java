package com.umesh.movie.service;

import com.umesh.movie.controller.request.BookingListRequest;
import com.umesh.movie.controller.request.NewBookingRequest;
import com.umesh.movie.controller.request.UpdateBookingRequest;
import com.umesh.movie.dto.model.admin.BookingDto;
import com.umesh.movie.dto.model.tmdb.ListDto;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 4:29 PM
 */

public interface AdminService {

    ListDto<BookingDto> fetchBookingList(BookingListRequest request);
    BookingDto updateBooking(Integer id, UpdateBookingRequest request);
    BookingDto newBooking(NewBookingRequest request);
}

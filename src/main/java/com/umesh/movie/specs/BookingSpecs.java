package com.umesh.movie.specs;

import com.umesh.movie.controller.request.BookingListRequest;
import com.umesh.movie.model.admin.Booking;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 5:59 PM
 */

public class BookingSpecs {

    public static Specification<Booking> requestFilters(final BookingListRequest request) {
        return (root, query, builder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(request)) {

                /* Query */
                if (StringUtils.isNotBlank(request.getQuery())) {
                    String queryStr = "%" + request.getQuery().toLowerCase() + "%";
                    predicates.add(
                            builder.or(
                                    builder.or(
                                            builder.like(
                                                    builder.lower(
                                                            root.get("firstName")
                                                    ), queryStr),
                                            builder.like(
                                                    builder.lower(
                                                            root.get("lastName")
                                                    ), queryStr)
                                    ),
                                    builder.like(
                                            builder.lower(
                                                    root.get("email")
                                            ), queryStr)
                            )
                    );
                }
            }
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}

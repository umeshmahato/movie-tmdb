package com.umesh.movie.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 6:32 PM
 */

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
@AllArgsConstructor
public class BookingListRequest {

    private int offset;
    private int pageSize;
    private String query;

}

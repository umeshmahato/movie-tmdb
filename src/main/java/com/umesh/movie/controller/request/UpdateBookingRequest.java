package com.umesh.movie.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 16/07/2022 - 12:02 AM
 */

@Getter
@Setter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
@AllArgsConstructor
public class UpdateBookingRequest {

    @Email
    private String email;
    private String firstName;
    private String lastName;
    private Boolean status;

}

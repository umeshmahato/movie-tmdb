package com.umesh.movie.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 16/07/2022 - 12:02 AM
 */

@Getter
@Setter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
@AllArgsConstructor
public class NewBookingRequest {

    @NotNull
    private Date date;
    @NotNull
    @Email
    private String email;
    private String firstName;
    private String lastName;
    @NotNull
    @Min(1)
    private Integer movieId;
    private String movieName;
    @NotNull
    @Min(1)
    private int seats;

}

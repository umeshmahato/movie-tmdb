package com.umesh.movie.controller;

import com.umesh.movie.dto.model.tmdb.ListDto;
import com.umesh.movie.dto.model.tmdb.MovieDto;
import com.umesh.movie.dto.model.tmdb.ReviewDto;
import com.umesh.movie.dto.response.Response;
import com.umesh.movie.dto.response.Status;
import com.umesh.movie.service.TmdbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 13/07/2022 - 10:48 PM
 */


@RestController
public class MovieDatabaseController {

    @Autowired
    TmdbService tmdbService;

    @GetMapping("/test")
    public Response<String> test() {

        Response<String> response = new Response();
        response.setStatus(Status.OK).setPayload("Hello World!");
        return response;

    }

    @GetMapping(path = {"api/movie/list/page/{page}", "api/movie/list/page/{page}/{query}"})
    public Response<ListDto<MovieDto>> getMovies(@PathVariable(name = "page") int page,
                                                 @PathVariable(name = "query", required = false) String query) {
        Response<ListDto<MovieDto>> response = new Response<>();

        ListDto<MovieDto> movieListDto = new ListDto<>();
        if (ObjectUtils.isEmpty(query))
            movieListDto = tmdbService.getMoviesByDiscovery(page);
        else movieListDto = tmdbService.getMoviesByQuery(page, query);

        if (ObjectUtils.isEmpty(movieListDto.getError())) {
            return response.setStatus(Status.OK)
                    .setStatusMsg("Successfully fetched movies!")
                    .setPayload(movieListDto);
        }
        return response.setStatus(Status.EXCEPTION).setStatusMsg(movieListDto.getError());
    }

    @GetMapping(path = {"api/movie/detail/{id}"})
    public Response<MovieDto> getMovieDetails(@PathVariable(name = "id") Long id) {

        Response<MovieDto> response = new Response<>();
        MovieDto movieDto = tmdbService.getMovieDetail(id);

        if (ObjectUtils.isEmpty(movieDto.getError())) {
            return response.setStatus(Status.OK)
                    .setStatusMsg("Successfully fetched movie detail!")
                    .setPayload(movieDto);
        }
        return response.setStatus(Status.EXCEPTION).setStatusMsg(movieDto.getError());
    }

    @GetMapping(path = {"api/movie/review/{id}"})
    public Response<ListDto<ReviewDto>> getMovieReviews(@PathVariable(name = "id") Long id) {

        Response<ListDto<ReviewDto>> response = new Response<>();
        ListDto<ReviewDto> reviewListDto = tmdbService.getMovieReviews(id);

        if (ObjectUtils.isEmpty(reviewListDto.getError())) {
            return response.setStatus(Status.OK)
                    .setStatusMsg("Successfully fetched reviews!")
                    .setPayload(reviewListDto);
        }
        return response.setStatus(Status.EXCEPTION).setStatusMsg(reviewListDto.getError());

    }

}

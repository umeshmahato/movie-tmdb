package com.umesh.movie.controller;

import com.umesh.movie.controller.request.BookingListRequest;
import com.umesh.movie.controller.request.NewBookingRequest;
import com.umesh.movie.controller.request.UpdateBookingRequest;
import com.umesh.movie.dto.model.admin.BookingDto;
import com.umesh.movie.dto.model.tmdb.ListDto;
import com.umesh.movie.dto.response.Response;
import com.umesh.movie.dto.response.Status;
import com.umesh.movie.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 1:29 PM
 */

@RestController
public class BookingController {

    @Autowired
    AdminService adminService;

    @GetMapping("api/booking/list")
    public Response<ListDto<BookingDto>> fetchBookings(@RequestParam int offset,
                                                       @RequestParam int pageSize,
                                                       @RequestParam(required = false) String query) {

        Response<ListDto<BookingDto>> response = new Response<>();
        ListDto<BookingDto> bookingListDto
                = adminService.fetchBookingList(new BookingListRequest(offset, pageSize, query));

        if (ObjectUtils.isEmpty(bookingListDto.getError())) {
            return response.setStatus(Status.OK)
                    .setStatusMsg("Successfully fetched bookings!")
                    .setPayload(bookingListDto);
        }
        return response.setStatus(Status.EXCEPTION).setStatusMsg(bookingListDto.getError());

    }

    @PutMapping("api/booking/{id}")
    public Response<BookingDto> updateBooking(@PathVariable(name = "id", required = false) Integer id,
                                              @RequestBody @Valid UpdateBookingRequest request) {
        Response<BookingDto> response = new Response<>();

        BookingDto bookingDto = adminService.updateBooking(id, request);
        if (ObjectUtils.isEmpty(bookingDto.getError())) {
            return response.setStatus(Status.OK)
                    .setStatusMsg("Successfully updated booking!")
                    .setPayload(bookingDto);
        }
        return response.setStatus(Status.EXCEPTION).setStatusMsg(bookingDto.getError());
    }

    @PostMapping("api/booking")
    public Response<BookingDto> newBooking(@RequestBody @Valid NewBookingRequest request) {
        Response<BookingDto> response = new Response<>();

        BookingDto bookingDto = adminService.newBooking(request);
        if (ObjectUtils.isEmpty(bookingDto.getError())) {
            return response.setStatus(Status.OK)
                    .setStatusMsg("Successfully saved booking!")
                    .setPayload(bookingDto);
        }
        return response.setStatus(Status.EXCEPTION).setStatusMsg(bookingDto.getError());
    }

}

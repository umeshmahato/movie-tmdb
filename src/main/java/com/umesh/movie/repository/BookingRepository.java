package com.umesh.movie.repository;

import com.umesh.movie.model.admin.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 7:31 AM
 */

public interface BookingRepository extends JpaRepository<Booking, Integer>,
        JpaSpecificationExecutor<Booking> {

    List<Booking> findByEmailAndMovieId(String email, Integer movieId);
}

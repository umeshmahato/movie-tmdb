package com.umesh.movie.dto.model.admin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 4:55 PM
 */

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
public class BookingDto {

    private Integer id;
    private String email;
    private String firstName;
    private String lastName;
    private Integer movieId;
    private String movieName;
    private Integer seats;
    private Timestamp date;
    private Timestamp createdAt;
    private boolean status;
    private String error;

}

package com.umesh.movie.dto.model.tmdb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 4:51 PM
 */

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
public class MovieDto {
    private Long id;
    private String language;
    private String title;
    private String overview;
    private String rating;
    private String poster;
    private String releaseDate;
    private String adult;
    private String voteCount;
    private List<String> genres;
    private String error;
}

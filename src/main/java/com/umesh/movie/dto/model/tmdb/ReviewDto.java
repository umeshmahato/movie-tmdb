package com.umesh.movie.dto.model.tmdb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 7:35 PM
 */

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
public class ReviewDto {

    private String author;
    private String content;
    private String createdAt;
    private String url;

}

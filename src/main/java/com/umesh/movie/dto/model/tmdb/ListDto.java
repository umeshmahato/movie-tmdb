package com.umesh.movie.dto.model.tmdb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 11:20 AM
 */

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
public class ListDto<T> {

    private Integer page;
    private Integer totalPages;
    private Integer totalResults;
    private List<T> results;
    private String error;

}

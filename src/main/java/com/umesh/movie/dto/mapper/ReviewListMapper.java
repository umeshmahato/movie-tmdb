package com.umesh.movie.dto.mapper;

import com.umesh.movie.dto.model.tmdb.ListDto;
import com.umesh.movie.dto.model.tmdb.ReviewDto;
import com.umesh.movie.model.tmdb.Reviews;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 11:39 PM
 */

public class ReviewListMapper {

    public static ListDto<ReviewDto> toReviewListDto(Reviews reviews) {
        return new ListDto<ReviewDto>()
                .setPage(reviews.getPage())
                .setTotalPages(reviews.getTotalPages())
                .setTotalResults(reviews.getTotalResults())
                .setResults(new ArrayList<>(reviews.getResults()
                        .stream()
                        .map(review -> {
                            ReviewDto reviewDto = new ReviewDto();
                            reviewDto.setAuthor(review.getAuthor());
                            reviewDto.setCreatedAt(review.getCreatedAt());
                            reviewDto.setUrl(review.getUrl());
                            reviewDto.setContent(review.getContent());
                            return reviewDto;
                        }).collect(Collectors.toList())
                ));
    }
}

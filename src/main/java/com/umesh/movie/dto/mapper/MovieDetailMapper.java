package com.umesh.movie.dto.mapper;

import com.umesh.movie.dto.model.tmdb.MovieDto;
import com.umesh.movie.model.tmdb.Genre;
import com.umesh.movie.model.tmdb.Result;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 7:14 PM
 */

public class MovieDetailMapper {
    public static MovieDto toMovieDto(Result result) {
        return new MovieDto()
                .setId(result.getId())
                .setReleaseDate(result.getReleaseDate())
                .setLanguage(result.getOriginalLanguage())
                .setRating(
                        ObjectUtils.isEmpty(result.getVoteAverage())
                                ? "0"
                                : result.getVoteAverage().toString()
                )
                .setTitle(result.getTitle())
                .setOverview(result.getOverview())
                .setPoster(result.getPosterPath())
                .setAdult(result.getAdult())
                .setVoteCount(
                        ObjectUtils.isEmpty(result.getVoteCount())
                                ? "0"
                                : result.getVoteCount().toString())
                .setGenres(
                        new ArrayList<>(result.getGenres()
                                .stream()
                                .map(Genre::getName)
                                .collect(Collectors.toList()))
                );
    }
}

package com.umesh.movie.dto.mapper;

import com.umesh.movie.dto.model.tmdb.ListDto;
import com.umesh.movie.dto.model.tmdb.MovieDto;
import com.umesh.movie.model.tmdb.Movies;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 14/07/2022 - 11:22 AM
 */

public class MovieListMapper {
    public static ListDto<MovieDto> toMovieListDto(Movies movies) {
        return new ListDto<MovieDto>()
                .setPage(movies.getPage())
                .setTotalPages(movies.getTotalPages())
                .setTotalResults(movies.getTotalResults())
                .setResults(new ArrayList<>(movies.getResults()
                        .stream()
                        .map(result -> {
                            MovieDto movieDto = new MovieDto();
                            movieDto.setId(result.getId());
                            movieDto.setLanguage(result.getOriginalLanguage());
                            movieDto.setTitle(result.getTitle());
                            movieDto.setOverview(result.getOverview());
                            if (!ObjectUtils.isEmpty(result.getVoteAverage()))
                                movieDto.setRating(result.getVoteAverage().toString());
                            movieDto.setPoster(result.getPosterPath());
                            movieDto.setAdult(result.getAdult());
                            movieDto.setReleaseDate(result.getReleaseDate());
                            return movieDto;
                        }).collect(Collectors.toList())));
    }
}

package com.umesh.movie.dto.mapper;

import com.umesh.movie.dto.model.admin.BookingDto;
import com.umesh.movie.dto.model.tmdb.ListDto;
import com.umesh.movie.model.admin.Booking;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 15/07/2022 - 5:22 PM
 */

public class BookingListMapper {
    public static ListDto<BookingDto> toBookingListDto(Page<Booking> bookings) {
        return new ListDto<BookingDto>()
                .setPage(bookings.getNumber())
                .setTotalPages(bookings.getTotalPages())
                .setTotalResults((int) bookings.getTotalElements())
                .setResults(new ArrayList<>(bookings.stream()
                        .map(booking -> {
                            BookingDto bookingDto = new BookingDto();
                            bookingDto.setId(booking.getId());
                            bookingDto.setEmail(booking.getEmail());
                            bookingDto.setFirstName(booking.getFirstName());
                            bookingDto.setLastName(booking.getLastName());
                            bookingDto.setMovieId(booking.getMovieId());
                            bookingDto.setMovieName(booking.getMovieName());
                            bookingDto.setSeats(booking.getSeats());
                            bookingDto.setDate(booking.getDate());
                            bookingDto.setCreatedAt(booking.getCreatedAt());
                            bookingDto.setStatus(booking.getStatus() == 1);
                            return bookingDto;
                        }).collect(Collectors.toList())));
    }
}

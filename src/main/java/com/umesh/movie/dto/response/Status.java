package com.umesh.movie.dto.response;

/**
 * @author toumesh@outlook.com
 * @project movie
 * @created 13/07/2022 - 11:27 PM
 */

public enum Status {
    OK, BAD_REQUEST, EXCEPTION
}

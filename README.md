# Springboot App for Movies Listing and Booking

Hosted at: https://movie-tmdb-practice.herokuapp.com

The API service connects to the https://www.themoviedb.org for movies details and stores booking informations in Postgres.


Tech Strategy:
```sh
Framework: Spring boot
Data storage: Postgres
Plugins: Lombok
```

API endpoints:

```sh
Movie List API 
GET /api/movie/list/page/{page}/{query}
Parameter query is optional

Movie Detail API 
GET /api/movie/detail/{id}

Movie Reviews API 
GET /api/movie/review/{id}

Booking List API 
GET /api/booking/list?offset={offset}&pageSize={pageSize}
GET /api/booking/list?offset={offset}&pageSize={pageSize}&query={query}

Update Booking API 
PUT /api/booking/{id}

Create Booking API 
POST /api/booking

```

Environment variables:

```sh
1. TMDB_API_KEY >> API KEY created in https://www.themoviedb.org/settings/api
2. POSTGRES_URL >> Postgres url in format jdbc:postgresql://<postgres_ip>:5432/<db_name>
3. POSTGRES_USER >> Postgres user name
4. POSTGRES_PASSWORD >> Postgres password
```

Install it and run:

```sh
mvn install
mvn spring-boot:run
```
